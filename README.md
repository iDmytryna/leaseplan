1.	Installation and run

Detailed instruction with creation and usage of the framework, consider in the Guideline.pdf file, attached to this project.

2.	Test Scenarios

A Feature File is an entry point to the Cucumber tests. This is a file where you will describe your tests in Gherkin language (Feature, Scenario, Given, When, And, and Then). These are keywords defined by Gherkin. 

Feature: Defines what feature you will be testing in the tests

Given: Tells the pre-condition of the test

And: Defines additional conditions of the test

Then: States the postcondition. You can say that it is the expected result of the test.

After that, you should implement Scenarios into code using WebDriverIO methods/commands from official documentation (https://webdriver.io/docs/api.html) and Page Object Model (all selectors or specific instructions that are unique for a certain page should be stored to Page Object).
