Feature: Cars filtering

The user should be able to filter cars by selecting Popular filters, Make&Model, MonthlyPrice, Term and Mileage, Fuel type and More filters 

Background: 
Given the browser is at Our Vehicles page

# Positive Scenarios
Scenario: Filter by selecting 'Best Deals' checkbox from Popular filters
    When the user search for 'Best Deals'
    Then results of related cars with 'Best Deals' mark are displayed

Scenario: Filter by selecting 'No stress plan' checkbox from Popular filters
    When the user search for 'No stress plan'
    Then results of related cars with a 'No stress plan' are displayed 

Scenario: Filter by selecting 'Configure yourself' checkbox from Popular filters
    When the user search for 'Configure yourself'
    Then results of related cars sums are displayed






Scenario: Filter by selecting any car from 'Popular Makes' checkbox of Make&Model filter
    When the user search for 'Mersedes A'
    Then results of related cars with 'Mersedes A' mark are displayed

Scenario: Filter by selecting any car from 'Other Makes' checkbox of Make&Model filter
    When the user search for 'EVOQUE Land Rover'
    Then results of related cars with a 'EVOQUE Land Rover' mark are displayed  






Scenario: Filter by selecting a MAX price from MonthlyPrice filters
    When the user search a MAX price of cars
    Then results of related cars with MAX price are displayed

Scenario: Filter by selecting a MIN price from MonthlyPrice filters
    When the user search a MIN price of cars
    Then results of related cars with MIN price are displayed






Scenario: Filter by selecting MIN value of Term(24 months) and Mileage filter
    When the user search MIN value of Term and Mileage 
    Then results of related cars with MIN value of Term - 24 months are displayed

Scenario: Filter by selecting AVG value of Term(36 months) and Mileage filter
    When the user search AVG value of Term - 36 months and Mileage 
    Then results of related cars with AVG value of Term - 36 months are displayed

Scenario: Filter by selecting AVG value of Term(48 months) and Mileage filter
    When the user search AVG value of Term - 48 months and Mileage 
    Then results of related cars with AVG value of Term - 48 months are displayed

Scenario: Filter by selecting MAX value of Term(60 months) and Mileage filter
    When the user search MAX value of Term - 60 months and Mileage 
    Then results of related cars with MAX value of Term - 60 months are displayed





Scenario: Filter by selecting any type from 'Fuel type' filter
    When the user search for 'Electric'
    Then results of related cars with 'Electric' fuel type are displayed





# Negative Scenarios
Scenario: Filter by selecting any value from unavailable checkbox
    When the user search for 'Audi' by using 'Best deals' filter
    Then no results of related cars with 'Audi' model are displayed





Scenario: Filter by selecting any value from unavailable checkbox
    When the user search for 'Tesla' model by using 'Configure Yourself' filter
    Then no results of related cars with 'Hybrid' fuel type are displayed