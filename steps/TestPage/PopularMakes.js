const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'Mersedes A'", ()=> {
    testpage.makeAndModel.click();
    testpage.mersedes.click();
    testpage.mersedesA.click();
    testpage.saveBtn.click();
    browser.pause(2000);
    })
    
Then("results of related cars with 'Mersedes A' mark are displayed", ()=> {
    if(testpage.mersedesASumText == '48 to choose from'){
        console.log(`Result of a 'Mersedes A' serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }           
})