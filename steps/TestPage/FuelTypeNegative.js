const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'Tesla' model by using 'Configure Yourself' filter", ()=> {
    testpage.PopularfiltersBtn.click();
    testpage.confYourselfBtn.click();
    testpage.makeAndModel.click();
    testpage.tesla.click();
    testpage.fuelType.click();
    testpage.hybrid.click();
    browser.pause(2000);
    })

Then("no results of related cars with 'Hybrid' fuel type are displayed", ()=> {
    console.log(testpage.hybrid.isSelected());                                  //Expected: False
})