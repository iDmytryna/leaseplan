const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'Electric'", ()=> {
    testpage.PopularfiltersBtn.click();
    testpage.fuelType.click();
    testpage.cng.click();
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with 'Electric' fuel type are displayed", ()=> {
    testpage.cngResultisDisplayed;          
})