const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'Audi' by using 'Best deals' filter", ()=> {
    testpage.PopularfiltersBtn.click();
    testpage.bestdeals.click();
    testpage.makeAndModel.click();
    testpage.audi.click();
    browser.pause(2000);
    })

Then("no results of related cars with 'Audi' model are displayed", ()=> {
    console.log(testpage.audi.isSelected());                                  //Expected: False
})